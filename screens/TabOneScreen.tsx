import { StyleSheet,Image } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';

export default function TabOneScreen({ navigation }: RootTabScreenProps<'TabOne'>) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Aplicativo Delivery Senac App</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(240,240,120,40)" />
      <Image 
      style={{width: 190, height:110, justifyContent: 'center'}}
      source={{uri: 'https://www.sp.senac.br/o/senac-theme/images/logo_senac_default.png'}} 
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 32,
    width: '80%',
  },
  iimages:{
    width: 60,
    height: 60,
    justifyContent: 'center',
    marginVertical: 30,
  }
});
